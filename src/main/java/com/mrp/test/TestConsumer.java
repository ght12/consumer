package com.mrp.test;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestConsumer {

	public static void main(String[] args) {
		try {
			ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-context.xml");
			context.start();
			 System.out.println("成功！！！");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}